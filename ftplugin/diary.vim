
" DESCRIPTION:  Diary filetype plugin file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Local settings

" use gf to navigate included files
setlocal suffixesadd=.diary,.md,.mkd
" set local tab width
setlocal tabstop=2
setlocal shiftwidth=2
setlocal softtabstop=2
" set wrapping long lines at a character in 'breakat'
setlocal linebreak
" hide special symbols
setlocal nolist
" make sure ':' is not a part of words
setlocal iskeyword-=:
" hide line numbers
setlocal norelativenumber
setlocal nonumber
" avoid slowdowns on long wrapped lines
" https://stackoverflow.com/questions/26815604/vim-syntax-performance-with-very-long-lines
setlocal synmaxcol=2048
syntax sync minlines=2048
" disable spelling
setlocal nospell
" re-set local marks
if g:diaryvim_set_marks
    autocmd! BufWrite <buffer> let b:mark_loc = getpos('.') | call diary#diary#ReadDiary() | call setpos('.', b:mark_loc) | unlet b:mark_loc
endif

" Local mappings

" navigate to the next diary file
nnoremap <buffer> <silent> dx :<C-u>execute "DiaryEntryNav " . v:count1<CR>
" navigate to the previous diary file
nnoremap <buffer> <silent> dz :<C-u>execute "DiaryEntryNav -" . v:count1<CR>
" use the inequality signs for the file navigation, too
if g:diaryvim_extended_mappings_navigation == 1
    " navigate to the next diary file
    nnoremap <buffer> <silent> > :<C-u>execute "DiaryEntryNav " . v:count1<CR>
    " navigate to the previous diary file
    nnoremap <buffer> <silent> < :<C-u>execute "DiaryEntryNav -" . v:count1<CR>
endif
" navigate to tomorrow's diary file (or to today's diary file first if needed)
nnoremap <buffer> <silent> dc :<C-u>DiaryEntry <C-r>=diary#diary#OpenTodaysOrTomorrowsDiary()<CR><CR><CR>gg
" or, map it this way
" nnoremap <buffer> <silent> <expr> dc diary#diary#OpenTodaysOrTomorrowsDiary()
"             \ ? ":DiaryEntry 1<CR><CR>"
"             \ : ":DiaryEntry<CR><CR>"

" navigate by section
nnoremap <buffer> <silent> [[ :call search('^##', 'b')<CR>:call search('^##', 'b')<CR>2j
nnoremap <buffer> <silent> ]] :call search('^##')<CR>2j

" navigate by item
nnoremap <buffer> <silent> ( :call search('^\s\+- ', 'b')<CR>
nnoremap <buffer> <silent> ) :call search('^\s\+- ')<CR>

" toggle goals
nnoremap <buffer> <silent> <C-d> ms:call diary#diary#ToggleGoals()<CR>`s
xnoremap <buffer> <silent> <C-d> ms:call diary#diary#ToggleGoals()<CR>`s

" toggle commits
if g:diaryvim_extended_mappings_toggle_commits == 1
    try
        silent set <M-d>?
    catch /E846/
        set <M-d>=d
    endtry
    nnoremap <buffer> <silent> <M-d> ms:call diary#diary#ToggleCommits()<CR>`s
    xnoremap <buffer> <silent> <M-d> ms:call diary#diary#ToggleCommits()<CR>`s
elseif g:diaryvim_extended_mappings_toggle_commits == 2
    nnoremap <buffer> <silent> <C-s> ms:call diary#diary#ToggleCommits()<CR>`s
    xnoremap <buffer> <silent> <C-s> ms:call diary#diary#ToggleCommits()<CR>`s
endif

" toggle TODOs
nnoremap <buffer> <silent> <C-t> ms:call diary#diary#ToggleTodos()<CR>`s
xnoremap <buffer> <silent> <C-t> ms:call diary#diary#ToggleTodos()<CR>`s

" the magic Spacebar (assumes code-related section being the second (between the second and the third headers))
xnoremap <buffer> <silent> <Space> ms:call diary#diary#TheMagicSpacebar()<CR>`s

" the magic Return (indents text after Notes section header)
inoremap <buffer> <silent> <expr> <CR> pumvisible()
            \ ? '<Esc>a'
            \ : getline('.')[0] == '>'
                \ ? '<CR><Tab>'
                \ : '<CR>'

" set awk
if executable('awk') && system('if awk --version | grep -q GNU; then echo 1; else echo 0; fi') == 1
    let GNUAWK = 1
else
    let GNUAWK = 0
    echohl ErrorMsg
    echo "ERROR: GNU awk not found, some functions will not be available"
    echohl None
endif
" view upcoming events (awk required)
if GNUAWK
    nnoremap <buffer> <silent> du :call diary#diary#ViewUpco()<CR>
endif

" view TODOs (awk required)
if GNUAWK
    nnoremap <buffer> <silent> dy :call diary#diary#ViewTodo()<CR>
endif

" view unfinished transfers (awk required)
if GNUAWK
    nnoremap <buffer> <silent> dr :call diary#diary#ViewTran()<CR>
endif

" view month transfers (awk required)
if GNUAWK
    nnoremap <buffer> <silent> dm :call diary#diary#ViewMonthTran()<CR>
endif

" fuzzyfind events and TODOs (fzf required)
if exists(":FZF")
    nnoremap <buffer> <silent> <C-g> :call fzf#run(fzf#wrap({
                \ 'source': "grep -i -r -H -n --color=never --include='*.diary' . " . g:diaryvim_location,
                \ 'sink': function('diary#diary#DiaryFZFFun'),
                \ 'down': '40%',
                \ 'options': ['--prompt', 'Diary> ']}))<CR>
    nnoremap <buffer> <silent> <Leader><C-g> :call fzf#run(fzf#wrap({
                \ 'source':"grep -i -r -H -n --color=never --include='*.diary' 'TODO:' " . g:diaryvim_location,
                \ 'sink': function('diary#diary#DiaryFZFFun'),
                \ 'down': '40%',
                \ 'options': ['--prompt', 'Diary> ']}))<CR>
    command! -nargs=+ -complete=dir DiaryFZF call fzf#run(fzf#wrap(fzf#vim#with_preview({
                \ 'source': "grep -i -r -H -n --color=never --include='*.diary' " . <q-args> . ' ' . g:diaryvim_location,
                \ 'sink': function('diary#diary#DiaryFZFFun'),
                \ 'down': '60%',
                \ 'options': '--prompt="Diary> "'})))
endif

" list entries with notes (rg required)
if executable('rg')
    if g:diaryvim_notes_list == 'loc'
        nnoremap <buffer> <silent> dq :lgetexpr systemlist("rg --vimgrep --smart-case --hidden --follow --multiline '^\n^>..' --glob '*.diary' " . g:diaryvim_location)<CR>:call setloclist(0, diary#diary#CompactL(getloclist(0)))<CR>:call setloclist(0, [], 'a', {'title': 'Notes'})<CR>
    elseif g:diaryvim_notes_list == 'qf'
        nnoremap <buffer> <silent> dq :cgetexpr systemlist("rg --vimgrep --smart-case --hidden --follow --multiline '^\n^>..' --glob '*.diary' " . g:diaryvim_location)<CR>:call setqflist(diary#diary#CompactL(getqflist()))<CR>:call setqflist([], 'a', {'title': 'Notes'})<CR>
    endif
endif

" Folding
setlocal foldlevel=0
setlocal foldmethod=expr
" function! DiaryFolding(lnum)
"     let line1 = getline(a:lnum)
"     let line2 = getline(a:lnum + 1)
"     if line1 =~ '^$'
"         if line2 =~# '^> .'
"             return ">2"
"         elseif line2 =~# '^\s\{' . &shiftwidth  . '}- [A-Z]' || line2 =~# '^\s\{' . &shiftwidth  . '}- +++ ' || line2 =~# '^> .'
"             return ">1"
"         elseif line2 =~ '^$' || line2 =~ '^#'
"             return 0
"         elseif foldlevel(a:lnum - 1) == 2
"             return 1
"         endif
"     endif
"     return "="
" endfunction
function! DiaryFolding(lnum)
    let line = getline(a:lnum)
    if line =~# '^> .'
        return ">1"
    elseif line =~ '^$'
        return "<1"
    endif
    return "="
endfunction
setlocal foldexpr=DiaryFolding(v:lnum)
