
" DESCRIPTION:  Diary filetype commands file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Check if loaded
if exists("g:ft_diary_loaded")
    finish
endif
let g:ft_diary_loaded = 1

" Define variables
if !exists("g:diaryvim_location")
    let g:diaryvim_location = "~/.diary"
endif
if !exists("g:diaryvim_template")
    let g:diaryvim_template = "~/.vim/plugged/vim-diary/template/template.diary"
endif
if !exists("g:diaryvim_pending")
    let g:diaryvim_pending = " - notes"
endif
if !exists("g:diaryvim_sets")
    let g:diaryvim_sets = ' - run\| - walk\| - push\| - plank\| - swim'
endif
if !exists("g:diaryvim_transactions_symbols")
    let g:diaryvim_transactions_symbols = '💳|💰'
endif
if !exists("g:diaryvim_extended_mappings_navigation")
    let g:diaryvim_extended_mappings_navigation = 1
endif
if !exists("g:diaryvim_extended_mappings_toggle_commits")
    " 0: no specific mappings added, toggling's managed by the Magic Spacebar
    " 1: <M-d> mappings added
    " 2: <C-s> mappings added
    let g:diaryvim_extended_mappings_toggle_commits = 1
endif
if !exists("g:diaryvim_notes_list")
    let g:diaryvim_notes_list = 'loc'
endif
if !exists("g:diaryvim_set_marks")
    let g:diaryvim_set_marks = 0
endif

" Define commands
command! -nargs=? DiaryEntry call diary#diary#CreateOrOpenDiaryEntry("open", <f-args>)
command! -nargs=1 DiaryEntryNav call diary#diary#CreateOrOpenDiaryEntry("navigate", <f-args>)
