
" DESCRIPTION:  Diary filetype detection file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Initialization
autocmd! BufEnter,BufRead,BufNewFile *.diary set filetype=diary
