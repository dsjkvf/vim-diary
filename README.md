Vim Diary
=========

## About

This is a bundle for `diary` filetype, which best suits for daily logs: to record what was planned, what achieved, which pending tasks and notes are still present, etc. Includes syntax highlighting, matching rules, template and some mappings local to the filetype.

## Dependencies and configurations

The core setting of the plugin is how it manages dates, which are used for the creation of diary files, and for the navigation between them. The plugin tries to detect if `date` command is available, and if it's a [GNU](http://man7.org/linux/man-pages/man1/date.1.html) implementation, or a [BSD](https://www.freebsd.org/cgi/man.cgi?date) one. If any of these are unavailable, the only other way for the plugin to function is to have Python support in Vim. The plugin stores this detected setting in `g:diaryvim_engine`, and it may be set to one of these three options: `bsddate`, `gnudate` or `python`. However, generally you don't have to set it manually (unless you are on Windows, probably).

Two other important options are the notes' location (`~/.diary` by default) and the location of the template (`~/.vim/plugged/vim-diary/template/template.diary` by default). In order to change those, please, define the variables `g:diaryvim_location` and `g:diaryvim_template` in your VIMRC accordingly.

Also, as shown on the screenshot, the plugin specifically highlights a collection of sets, which in this particular case are the series of some physical exercises. The prefixes for this activities are stored in the global variable `g:diaryvim_sets` and by default it is set to `' - run\| - walk\| - push\| - plank\| - swim'`.

Besides, the plugin also highlights a specific set of pending notes, which are links to either `.md` or `.diary` files (or any other filetype, actually), and by default are stored in the subdirectory named `notes`. However, the prefix can be changed by defining the variable `g:diaryvim_pending` in your VIMRC (like this: `let g:diaryvim_pending = ' - notes'`)

The plugin also introduces a command, `DiaryEntry`, either to open the current diary entry, or to create a new one (with the help of the template file, `template/template.diary`). This command can be used together with the [vim-startify](https://github.com/mhinz/vim-startify) plugin, for example.

Besides, to tweak categories and colors even more, please, feel free to inspect and to edit the `syntax/diary.vim` file.

## Mappings

  - `%` and `g%` cycle forward and backward through groups `Goals/Sets` and `Tasks/Code/Pending/Notes` (see `:help matchit`);
  - `>` and `<` (or `dz` and `dq`) navigate forward and backward through day files;
    - if you want to preserve the original mappings for the inequality signs, set the `g:diaryvim_inequality_signs_mappings` variable to `0` in your `$VIMRC`
  - `C-d` strikes out an item in a list;
  - `C-t` transforms an item in a list into a TODO entry;
  - if [fzf](https://github.com/junegunn/fzf.vim) is installed, `C-g` greps the directory, and `<Leader>C-g` greps the directory for TODO entries.

## Installation

Copy the provided files to your `$VIMHOME` directory, or simply use any plugin manager available.

## Screenshots

![example](http://i.imgur.com/M1pkzEG.png)

## License

Copyright (c) dsjkvf unless stated otherwise. Distributed under the same terms as Vim itself. See `:help license`.
