
" DESCRIPTION:  Diary autoload plugin file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" HELPERS

" Preset some local bookmarks when opening a diary file (feel free to tweak and to add other possible routines)
function! diary#diary#ReadDiary()
    let lines =[]
    silent! %s/^## \zs.*$/\=len(add(lines, line('.'))) ? line('.') : ''/ne
    call histdel('search', -1)
    silent! execute 'normal!' . lines[1] . 'G{{k$ma'
    silent! execute 'normal!' . lines[1] . 'G{k$mw'
    silent! execute 'normal!' . lines[2] . 'Gkk$mo'
    call setpos('.', [0, 0, 0, 0])
endfunction

" MAIN

" Create a new diary file / open an existing one
function! diary#diary#CreateOrOpenDiaryEntry(purpose, ...)
    if a:purpose == "open"
        let the_date = strftime("%Y-%m-%d")
    elseif a:purpose == "navigate"
        let the_date = expand("%<")
    else
        echohl WarningMsg
        echom "Wrong argument(-s) provided!"
        echohl NormalMsg
    endif

    if a:0 == 0
        let delta = 0
    elseif a:0 == 1
        if a:1 =~ '^-\?\d\+$' && len(a:1) < 4
            let delta = a:1
        elseif a:1 =~ '2\d\{7}'
            let the_date = a:1[0:3] . '-' . a:1[4:5] . '-' . a:1[6:7]
            let delta = 0
        elseif a:1 =~ '2\d\d\d-\d\d-\d\d'
            let the_date = a:1
            let delta = 0
        else
            echohl WarningMsg
            echom "Wrong argument(-s) provided!"
            echohl NormalMsg
            return
        endif
    else
        echohl WarningMsg
        echom "Wrong argument(-s) provided!"
        echohl NormalMsg
        return
    endif

    let epoch_date = strptime('%Y-%m-%d', the_date)
    let next_diary = strftime("%Y-%m-%d", epoch_date + delta * 86400)
    " check if it's not a DST turning on/off date
    if the_date == next_diary
        if strftime('%m', localtime()) == '10' || strftime('%m', localtime()) == '11'
            let next_diary = strftime('%Y-%m-%d', epoch_date + delta * 86400 + 3600)
        elseif strftime('%m', localtime()) == '03' || strftime('%m', localtime()) == '04'
            let next_diary = strftime('%Y-%m-%d', epoch_date + delta * 86400 - 3600)
        endif
    endif
    let next_diary_title = strftime("%a %d %b %Y", strptime('%Y-%m-%d', next_diary))

    if has('win32') || has ('win64')
        if g:diaryvim_location[-1:-1] != '\'
            let g:diaryvim_location = g:diaryvim_location . '\'
        endif
    else
        if g:diaryvim_location[-1:-1] != '/'
            let g:diaryvim_location = g:diaryvim_location . '/'
        endif
    endif

    let next_diary_file = g:diaryvim_location . next_diary . '.diary'

    if !empty(glob(next_diary_file))
        execute "edit " . next_diary_file
    else
        if a:purpose == 'navigate'
            echohl Question
            echom "File doesn't exist. Create? [y]/n "
            echohl None
            silent let create_new = nr2char(getchar())
            redraw!
        elseif a:purpose == 'open'
            let create_new = 'y'
        endif
        if create_new == '' || create_new == 'y'
            execute 'edit ' . next_diary_file
            put! =next_diary_title
            execute 'read ' . g:diaryvim_template
            silent! normal G"_ddgg
            write
        else
            return
        endif
    endif
    if g:diaryvim_set_marks == 1
        call diary#diary#ReadDiary()
    endif
    if !empty(expand('#'))
        silent! buffer #
        if &filetype == 'diary'
            if &modified == 1
                let l:mod = 1
            else
                let l:mod = 0
            endif
            silent! buffer #
            if l:mod == 0
                silent! bwipeout! #
            endif
        else
            silent! buffer #
        endif
    endif
endfunction

" Create / open tomorrow's diary file (or navigate to today's diary file first if needed)
function! diary#diary#OpenTodaysOrTomorrowsDiary()
    let the_date_comp = strftime("%Y-%m-%d")
    let the_date_file = expand("%<")
    if the_date_file != the_date_comp
        return 0
    elseif the_date_file == the_date_comp
        return 1
    else
        echom 'This should not happen; see vim-diary/ftplugin/diary.vim for details'
        return 'Error'
endfunction

" Toggle tasks

" toggle goals
function! diary#diary#ToggleGoals()
    if len(getline('.')) > shiftwidth()
        if getline('.') !~ '+++'
            silent substitute/\(^[-* ]\+\)/\1+++ /
        else
            silent substitute/+++ //
        endif
        call histdel('search', -1)
    endif
endfunction

" toggle commits
function! diary#diary#ToggleCommits()
    if len(getline('.')) > shiftwidth()
        if getline('.') !~ ' - %'
            silent substitute/\(^[^0-9A-Za-z_'"<]\+\)/\1% /
        else
            silent substitute/% //
        endif
        call histdel('search', -1)
    endif
endfunction

" toggle TODOs
function! diary#diary#ToggleTodos()
    let the_lin = getline('.')
    if len(the_lin) > shiftwidth()
        if the_lin !~# 'TODO:' && the_lin !~# 'WAIT:' && the_lin !~# 'DONE:'
            silent substitute/\(^\W\+\)/\1TODO: /
        elseif the_lin =~# 'TODO:'
            silent substitute/TODO: /WAIT: 
        elseif the_lin =~# 'WAIT'
            silent substitute/WAIT: /DONE: 
        elseif the_lin =~# 'DONE:'
            silent substitute/DONE: //
        endif
        call histdel('search', -1)
    endif
endfunction

" the magic Spacebar
function! diary#diary#TheMagicSpacebar() range
    let lines =[]
    silent! %s/^## \zs.*$/\=len(add(lines, line('.'))) ? line('.') : ''/ne
    call histdel('search', -1)
    if len(lines) >= 2
        if a:firstline > lines[0] && a:lastline < lines[1]
            for l in range(a:firstline, a:lastline)
                call setpos('.', [ 0, l, 0, 0 ])
                call diary#diary#ToggleGoals()
            endfor
        elseif a:firstline > lines[1] && a:lastline < lines[2]
            for l in range(a:firstline, a:lastline)
                call setpos('.', [ 0, l, 0, 0 ])
                if indent(l) == shiftwidth()
                    call diary#diary#ToggleGoals()
                else
                    call diary#diary#ToggleCommits()
                endif
            endfor
        elseif a:firstline > lines[-1]
            for l in range(a:firstline, a:lastline)
                call setpos('.', [ 0, l, 0, 0 ])
                call diary#diary#ToggleGoals()
            endfor
        endif
    endif
endfunction

" EXTRAS

" View

" view upcoming events (awk required)
function! diary#diary#ViewUpco()
    new
    set buftype=nofile
    set filetype=diary-agn
    setlocal wrap
    setlocal suffixesadd=.diary
    let @t = join(split(system("awk -v p=0 '{if (/Tasks/) {p=1}; if (/Sets/) {p=0; nextfile}; if (p && !/\+\+\+|Tasks|Sets|TODO:|^$/) {sub(\".diary\", \"\", FILENAME); b[FILENAME]=FILENAME; a[FILENAME][NR] = $0}} END{n=asorti(a, ai); for (i=1; i<=n; i++) {print ai[i]; for (j in a[ai[i]]) {print a[ai[i]][j]}}}' *.diary"), "\n"), "\n")
    put! t
endfunction

" view upcoming TODOs (awk required)
function! diary#diary#ViewTodo()
    new
    set buftype=nofile
    set filetype=diary-agn
    setlocal wrap
    setlocal suffixesadd=.diary
    let @t = join(split(system("awk -v p=0 '{if (/TODO:/) {p=1}; if (p && /^$/) {p=0};if (p) {sub(\".diary\", \"\", FILENAME); b[FILENAME]=FILENAME; a[FILENAME][NR] = $0}} END{n=asorti(a, ai); for (i=1; i<=n; i++) {print ai[i]; for (j in a[ai[i]]) {print a[ai[i]][j]}}}' *.diary"), "\n"), "\n")
    put! t
endfunction

" view unfinished transfers (awk required)
function! diary#diary#ViewTran()
    new
    set buftype=nofile
    set filetype=diary-agn
    setlocal wrap
    setlocal suffixesadd=.diary
    let @t = join(split(system("awk '{if (FNR==1){n=0; p=0}} {if (/Notes/) {p=1}} {if (/" . g:diaryvim_transactions_symbols . "/ && !/++/ && p) {if (!n) {printf " . '"\n"; f=FILENAME; sub(".diary", "", f); print f; n=1} sub(/^> abattoir:/,""); print}}' . "' *.diary"), "\n"), "\n")
    put! t
endfunction

" view this month transfers (awk required)
function! diary#diary#ViewMonthTran()
    let month = expand('%:%p:%t')[0:7]
    new
    set buftype=nofile
    set filetype=diary-agn
    setlocal wrap
    setlocal suffixesadd=.diary
    let @t = join(map(split(system("awk '{if (FNR==1){p=0}} {if (/Notes/) {p=1}} {if (p && /" . g:diaryvim_transactions_symbols . "/) {print}}' " . month . "*.diary"), "\n"), {idx, val -> substitute(substitute(val, '  - \(+++ \)*[' . join(split(g:diaryvim_transactions_symbols, '|'), '') . ']', '', ''), '\(\d\d\d\d\)\(\d\d\)', '\1.\2', '')}), "\n")
    put! t
endfunction

" Grep

" fuzzyfind events and TODOs (grep, fzf required)
function! diary#diary#DiaryFZFFun(file)
    let file_name_and_pos = split(a:file, ':')
    execute 'tabedit ' . file_name_and_pos[0]
    let t:startify_new_tab = 1
    if len(file_name_and_pos) > 1
        call setpos('.', [bufnr('.')[0], file_name_and_pos[1], 1])
    endif
endfunction

" compact (remove empty entries) already present QucikFix or Location lists (to narrown the lists down)
function! diary#diary#CompactL(l)
    let rez = []
    for i in range(0, len(a:l)-1)
        if a:l[i]['col'] != 0
            call add(rez, a:l[i])
        endif
    endfor
    return rez
endfunction
