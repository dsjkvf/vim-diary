
" DESCRIPTION:  Diary syntax file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Init
if exists("b:current_syntax")
  finish
endif

" Define groups
" helper
syn match   diaryLineBreak  /  \+$/
" headers
syn match   diaryHeader     /^.\+\n=\+$/                            contains=@noSpell,diaryLineBreak
syn match   diaryHeaderUnd  /^[=-]\+$/                              contains=@noSpell
syn match   diaryH2         /^##\s.*/                               contains=@noSpell
" general items
syn region  diaryComment    start=/^\s*<\!--/         end=/-->/
syn region  diaryStrike     start=/+++/               end=/$/
syn region  diaryCode       start=/ % /               end=/$/
" todos
syn region  diaryTodo       start=/TODO: /            end=/$/
syn region  diaryTodoDone   start=/DONE: /            end=/$/
" categories' items
execute 'syn region diarySets        start=/' . g:diaryvim_sets . '/ end=/\n/'
syn region  diaryFiles      start=/^\s*- file:/       end=/\n/
syn region  diaryNote       start=/^> /               end=/\n\n/      contains=diaryTodo,diaryTodoDone,diaryStrike

" Colorize
hi def link diaryHeader     Title
hi def link diaryHeaderUnd  Title
hi def link diaryH2         Title
hi def link diaryComment    Comment
hi def link diaryStrike     Comment
hi def link diaryCode       Comment
hi def link diaryTodo       Todo
hi def link diaryTodoDone   Comment
hi def link diarySets       Type
hi def link diaryFiles      String
hi def link diaryNote       Special
