
" DESCRIPTION:  Diary syntax file
" MAINTAINER:   dsjkvf <dsjkvf@gmail.com>

" Init
if exists("b:current_syntax")
  finish
endif

" Define groups
" headers
syn match   diaryHeaderAgn  /^\d\d\d\d-\d\d-\d\d$/
" todos
syn region  diaryTodoAgn    start=/TODO: /            end=/$/

" Colorize
hi def link diaryHeaderAgn  diaryHeader
hi def link diaryTodoAgn    diaryTodo
